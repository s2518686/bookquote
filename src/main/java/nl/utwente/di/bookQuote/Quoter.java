package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    private Map<String, Double> book_prices;

    public Quoter() {
        book_prices = new HashMap<>();
        book_prices.put("1", 10.0);
        book_prices.put("2", 45.0);
        book_prices.put("3", 20.0);
        book_prices.put("4", 35.0);
        book_prices.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        return (book_prices.containsKey(isbn)? book_prices.get(isbn): 0.0);
    }
}
